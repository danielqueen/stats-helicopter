% Created 2016-09-16 Fri 10:49
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[margin=1in]{geometry}
\author{Kawin Nikomborirak, Daniel Queen}
\date{\today}
\title{Paper Helicopters}
\hypersetup{
 pdfauthor={Kawin Nikomborirak, Daniel Queen},
 pdftitle={Paper Helicopters},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 24.5.1 (Org mode 8.3.6)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Conjecture}
\label{sec:orgheadline1}
This experiment will test whether success in dropping a paper helicopter onto a target is independent of the target's color.
The explanatory variable will be the color of the target, which will be either violet or red.
The response variable will be whether or not the subject hits the target with the paper helicopter.

\section{Design}
\label{sec:orgheadline5}

\subsection{Summary}
\label{sec:orgheadline2}
\begin{description}
\item[{H\(_{\text{0}}\)}] Target color (red or violet) and success (hit or miss target) are independent.
\item[{H\(_{\text{a}}\)}] Target color (red or violet) and success (hit or miss target) are not independent.
\item[{Treatments}] Drop helicopter onto either red or violet target
\item[{Random Assignment}] Use a random number generator.
If the number is odd, have the volunteer drop helicopter onto red target.
If the number is even, have the volunteer drop the helicopter onto the violet target.
\item[{Blocking}] upperclassman or underclassman.
\end{description}

\subsection{Materials}
\label{sec:orgheadline3}
\begin{itemize}
\item 2 targets of equal size
\begin{itemize}
\item 1 red
\item 1 violet
\end{itemize}
\item 1 large binder clip
\item 1 helicopter with a paperclip attached to the bottom
\begin{itemize}
\item The paperclip to make the helicopter reasonably controllable.
\end{itemize}
\item 1 random number generator
\end{itemize}

\subsection{Procedure}
\label{sec:orgheadline4}
\begin{enumerate}
\item Stack targets underneath foyer in 200 building.
Keep the targets there for the whole experiment.

\item Position one person by the target, and another on the foyer.

\item Have a person find a volunteer.

\item Use a random number generator to decide what color target should be displayed on top.
If odd, place the red target on top of the stack.
If even, place the violet target on top of the stack.

\item Ask the volunteer to stand on the foyer and drop the helicopter onto the target.

\item Have the person on the foyer pass the person by the target a binder clip to clip onto the helicopter with the blades secured.
Have the person by the targets toss the helicopter and binder clip back up to the person on the foyer (the clip makes the helicopter easier to toss).

\item Remove the binder clip.

\item Add a tally mark in the appropriate table cell, noting whether the volunteer was a upperclassman or an underclassman.

\item Repeat steps 2--5 until expected cell counts are all at least 5 (check the numbers when appropriate).
\end{enumerate}

\section{Data}
\label{sec:orgheadline9}
\subsection{General Results}
\label{sec:orgheadline6}
\begin{center}
\label{tab:orgtable1}
\begin{tabular}{lrr}
Outcome & Violet & Red\\
\hline
Hit & 12 & 10\\
Miss & 7 & 8\\
\end{tabular}
\end{center}

\begin{minted}[frame=lines]{r}
general <- general[,2:3]
dimnames(general) = list(Outcome=c('Hit','Miss'),
                       Treatments=c('Violet','Red'))
barplot(data.matrix(general),
        beside=T, legend=T,
        main='Success by Target Color',
        ylab='Frequency (# hit or miss)',
        xlab='Treatment (Target Color)')
\end{minted}

\includegraphics[width=.9\linewidth]{graphic-comparison.jpg}

\subsection{Underclassmen Results}
\label{sec:orgheadline7}
\begin{center}
\label{tab:orgtable2}
\begin{tabular}{lrr}
Outcome & Violet & Red\\
\hline
Hit & 4 & 4\\
Miss & 4 & 1\\
\end{tabular}
\end{center}

\begin{minted}[frame=lines]{r}
underclassmen <- underclassmen[,2:3]
dimnames(underclassmen) = list(Outcome=c('Hit','Miss'),
                       Treatments=c('Violet','Red'))
barplot(data.matrix(underclassmen),
        beside=T, legend=T,
        main='Underclassmen Success by Target Color',
        ylab='Frequency (# hit or miss)',
        xlab='Treatment (Target Color)')
\end{minted}

\includegraphics[width=.9\linewidth]{underclassmen-comparison.jpg}

\subsection{Upperclassmen Results}
\label{sec:orgheadline8}
\begin{center}
\label{tab:orgtable3}
\begin{tabular}{lrr}
Outcome & Violet & Red\\
\hline
Hit & 8 & 6\\
Miss & 3 & 7\\
\end{tabular}
\end{center}

\begin{minted}[frame=lines]{r}
upperclassmen <- upperclassmen[,2:3]
dimnames(upperclassmen) = list(Outcome=c('Hit','Miss'),
                       Treatments=c('Violet','Red'))
barplot(data.matrix(upperclassmen),
        beside=T, legend=T,
        main='Upperclassmen Success by Target Color',
        ylab='Frequency (# hit or miss)',
        xlab='Treatment (Target Color)')
\end{minted}

\includegraphics[width=.9\linewidth]{upperclassmen.jpg}

\section{Statistical Analyses}
\label{sec:orgheadline13}

\subsection{General Results}
\label{sec:orgheadline10}
All expected cell counts are all at least 5.
In order to apply a \(\chi^{\text{2}}\) test of independence on the data, we will assume the data is unbiased.

\begin{minted}[frame=lines]{r}
results <- chisq.test(general[,2:3], correct=F)
print(results$expected)
print(results)
\end{minted}

\begin{verbatim}
        Violet       Red
[1,] 11.297297 10.702703
[2,]  7.702703  7.297297

    Pearson's Chi-squared test

data:  general[, 2:3]
X-squared = 0.22162, df = 1, p-value = 0.6378

\end{verbatim}

\subsection{Underclassmen Results}
\label{sec:orgheadline11}
In order to apply a \(\chi^{\text{2}}\) test of independence on the data, we will assume the data is unbiased.

\begin{minted}[frame=lines]{r}
results <- chisq.test(underclassmen[,2:3], correct=F)
print(results$expected)
print(results)
\end{minted}

\begin{verbatim}
       Violet      Red
[1,] 4.923077 3.076923
[2,] 3.076923 1.923077

    Pearson's Chi-squared test

data:  underclassmen[, 2:3]
X-squared = 1.17, df = 1, p-value = 0.2794

\end{verbatim}

\subsection{Upperclassmen Results}
\label{sec:orgheadline12}
In order to apply a \(\chi^{\text{2}}\) test of independence on the data, we will assume the data is unbiased.

\begin{minted}[frame=lines]{r}
results <- chisq.test(upperclassmen[,2:3], correct=F)
print(results$expected)
print(results)
\end{minted}

\begin{verbatim}
       Violet      Red
[1,] 6.416667 7.583333
[2,] 4.583333 5.416667

    Pearson's Chi-squared test

data:  upperclassmen[, 2:3]
X-squared = 1.7311, df = 1, p-value = 0.1883

\end{verbatim}

\section{Conclusion}
\label{sec:orgheadline15}
For the general results, the \texttt{p-value} (0.892) is greater than the \texttt{significance value} (0.05).
Therefore, H\(_{\text{0}}\) fails to be rejected, meaning the target color (red or violet) and success (hit or miss target) are independent.

For the underclassmen results, the \texttt{p-value} (0.2794) is greater than the \texttt{significance value} (0.05).
Therefore, in this case, H\(_{\text{0}}\) fails to be rejected, meaning the target color (red or violet) and success (hit or miss target) are independent.

For the underclassmen results, the \texttt{p-value} (0.2794) is greater than the \texttt{significance value} (0.05).
Therefore, in this case, H\(_{\text{0}}\) fails to be rejected, meaning the target color (red or violet) and success (hit or miss target) are independent.

\subsection{Notes}
\label{sec:orgheadline14}
In all cases, H\(_{\text{0}}\) failed to be rejected, indicating that success in dropping a helicopter onto a target is independent of the target's color.

In the beginning of the experiment, the majority of the volunteers missed the target regardless of target color.
In the end of the experiment, volunteers who missed the target were rare.
At the time, there were more helicopter-related experiments taking place, so perhaps the volunteers near the end of our experiment were more practiced.
The amount of experience our volunteers had with similar experiments possibly became an extraneous factor.
\end{document}
